# Pump Control System

This Arduino-based pump control system allows users to automate the operation of a pump based on predefined time settings or manually control it. This README provides an overview of the program structure, usage instructions, and configuration options.


<pre>
██████╗ ██╗   ██╗███╗   ███╗██████╗      ██████╗ ██████╗ ███╗   ██╗████████╗██████╗  ██████╗ ██╗         ███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗
██╔══██╗██║   ██║████╗ ████║██╔══██╗    ██╔════╝██╔═══██╗████╗  ██║╚══██╔══╝██╔══██╗██╔═══██╗██║         ██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║
██████╔╝██║   ██║██╔████╔██║██████╔╝    ██║     ██║   ██║██╔██╗ ██║   ██║   ██████╔╝██║   ██║██║         ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║
██╔═══╝ ██║   ██║██║╚██╔╝██║██╔═══╝     ██║     ██║   ██║██║╚██╗██║   ██║   ██╔══██╗██║   ██║██║         ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║
██║     ╚██████╔╝██║ ╚═╝ ██║██║         ╚██████╗╚██████╔╝██║ ╚████║   ██║   ██║  ██║╚██████╔╝███████╗    ███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║
╚═╝      ╚═════╝ ╚═╝     ╚═╝╚═╝          ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝
</pre>
             


## Overview

The program consists of Arduino code that runs on an Arduino microcontroller board. It utilizes various components such as buttons, LEDs, and a liquid crystal display (LCD) for user interaction and feedback. The system can operate in two modes: automatic and manual.

+ Pump Control System Operation Block

  ```
  +-- Start
      |
      +-- Setup
      |    |
      |    +-- SetParameters
      |         |
      |         +-- ReadRTC
      |              |
      |              +-- DisplayWelcome
      |                   |
      |                   +-- WaitInput --[Yes]--> SerialInput --[UpdateDisplay]--> End
      |                               |
      |                               +--[No]---> CheckMode --[Auto]---> PerformAutoMode --> End
      |                                                |
      |                                                +--[Manual]--> PerformManualMode --> End
      |
      +-- UpdateDisplay --> End
  ```

  This block illustrates the sequential operation of the Pump Control System.
 ```
+-----------------------------------------------+
|                  setup()                      |
|                                               |
|   - Initialize pins and peripherals           |
|   - Begin serial communication                |
|   - Initialize RTC and set sync provider      |
|   - Print welcome message and instructions    |
|   - Initialize LCD and display project name   |
|   - Clear LEDs, set initial status            |
|                                               |
+----------------+------------------------------+
                 |
                 v
     +-----------------------+
     |        loop()         |
     |                       |
     |   - Handle settings   |
     |   - Update LCD        |
     |   - Toggle LEDs       |
     |   - Check and select  |
     |     operating mode    |
     |   - Execute mode      |
     |     functionality     |
     |                       |
     +-----------------------+
                 |
                 v
    +-------------------------+
    |       Settings()        |
    |                         |
    |   - Read serial input   |
    |   - Check input format  |
    |   - Set time settings   |
    |                         |
    +-------------------------+
                 |
                 v
   +---------------------------+
   |        mode_auto()        |
   |                           |
   |   - Check if time matches |
   |     setting for auto mode |
   |   - Update pump status    |
   |   - Turn pump on/off      |
   |                           |
   +---------------------------+
                 |
                 v
   +---------------------------+
   |       mode_manual()       |
   |                           |
   |   - Check button status   |
   |   - Toggle pump status    |
   |   - Update pump and LEDs  |
   |                           |
   +---------------------------+
 ```

## Components

- Arduino microcontroller board
- Real-Time Clock (RTC) module
- Buttons for mode selection and manual control
- LEDs for status indication
- Liquid crystal display (LCD) for displaying information

## Dependencies

The following libraries are required to run the code:
- DS3232RTC library: For RTC functionality
- LiquidCrystal_I2C library: For LCD control
- Streaming library: For Serial communication

## Usage

1. **Setting Time for Pump**: Send the following command through the serial interface to set the time for pump operation:
Replace `HH`, `MM`, and `SS` with the desired hour, minute, and second respectively.
To set the time for the pump, use the following command:
```
set time pump HH:MM:SS
Example: set time pump 12:30:00

```
2. **Setting Real-Time Clock (RTC)**: Use the command:
Replace `YY`, `MM`, `DD`, `HH`, `MM`, and `SS` with the year, month, day, hour, minute, and second respectively.
```
set time real YY:MM:DD:HH:MM:SS
Example: set time real 24:01:01:12:30:00

```

3. **Setting Last-Time stop**: Use the command:
Replace `HH`hour respectively.
```
set stop last hour HH
Example: set stop last hour 6

```

Please ensure that you enter the time in the correct format. If the input is invalid, you will receive an error message via the Serial Monitor.


4. **Selecting Operation Mode**: Use the physical switches connected to the Arduino board to select the operation mode:
- Auto Mode: Automatically controls the pump based on predefined time settings.
- Manual Mode: Allows manual control of the pump.

5. **Manual Pump Control**: In manual mode, press the designated button to toggle the pump on/off.

```
+-----------------------------------------+
| Check if the input starts with          |
| "set time pump"                         |
|                                         |
|                                         |
|      +-------------------+              |
|      | Yes                |             |
|      |                    |             |
|      | Extract hour,      |             |
|      | minute, and second |             |
|      | from the input     |             |
|      | string             |             |
|      +--------------------              |
|                                         |
|      +------------------+               |
|      | Validate the      |              |
|      | extracted values: |              |
|      |                   |              |
|      | Ensure the hour   |              |
|      | is between 0 and  |              |
|      | 23                |              |
|      | Ensure the minute |              |
|      | is between 0 and  |              |
|      | 59                |              |
|      | Ensure the second |              |
|      | is between 0 and  |              |
|      | 59                |              |
|      +-------------------               |
|                                         |
|      +------------------+               |
|      | Values are valid  |              |
|      |                   |              |
|      | Update the time   |              |
|      | settings for the  |              |
|      | pump              |              |
|      |                   |              |
|      | Print a success   |              |
|      | message with the  |              |
|      | new time settings |              |
|      +-------------------               |
|                                         |
|      +-------------------+              |
|      | Values are not    |              |
|      | valid             |              |
|      |                   |              |
|      | Print an error    |              |
|      | message           |              |
|      +-------------------+              |
+-----------------------------------------+
```


6. **Auto Pump Control**: In auto mode, the operation of the pump depends on the set time.
```
+-----------------------------------------------+
| Check if the input starts with                |
| "set time real"                               |
|                                               |
|                                               |
|      +------------------------+               |
|      | Yes                    |               |
|      |                        |               |
|      | Extract year, month,   |               |
|      | day, hour, minute, and |               |
|      | second from the input  |               |
|      | string                 |               |
|      +-------------------------               |
|                                               |
|      +----------------------+                 |
|      | Validate the          |                |
|      | extracted values:     |                |
|      |                       |                |
|      | Ensure the year is    |                |
|      | between 0 and 99      |                |
|      | Ensure the month is   |                |
|      | between 1 and 12      |                |
|      | Ensure the day is     |                |
|      | between 1 and 31      |                |
|      | Ensure the hour is    |                |
|      | between 0 and 23      |                |
|      | Ensure the minute is  |                |
|      | between 0 and 59      |                |
|      | Ensure the second is  |                |
|      | between 0 and 59      |                |
|      +-----------------------                 |
|                                               |
|      +----------------------+                 |
|      | Values are valid      |                |
|      |                       |                |
|      | Update the real-time  |                |
|      | clock settings        |                |
|      |                       |                |
|      | Print a success       |                |
|      | message with the new  |                |
|      | date and time settings|                |
|      +-----------------------                 |
|                                               |
|      +------------------------+               |
|      | Values are not valid    |              |
|      |                         |              |
|      | Print an error message  |              |
|      +-------------------------               |
+-----------------------------------------------+
```

## Configuration

The program can be configured using the `config.h` file. Here are some configurable parameters:
- Pins for mode selection switches, buttons, and LEDs
- Time intervals for LCD backlight and debounce settings

## Additional Notes

- Ensure all required libraries are installed in the Arduino IDE before uploading the code.
- Serial communication is used for configuration and debugging purposes. Make sure the baud rate is set to 9600.

For more details on the code implementation, refer to the source code files (`config.h` and the main sketch).

**Creator:** "42595F4348415455524F4E475F4B4841"
**Author:** chaturong
**Email:** chaturong@narit.or.th
Feel free to reach out for assistance or to report any issues. Happy exploring!
  ```

 ██████╗██╗  ██╗ █████╗ ████████╗██╗   ██╗██████╗  ██████╗ ███╗   ██╗ ██████╗     ██╗  ██╗██╗  ██╗ █████╗  ██████╗██╗  ██╗ █████╗ ██████╗  █████╗ ███╗   ██╗
██╔════╝██║  ██║██╔══██╗╚══██╔══╝██║   ██║██╔══██╗██╔═══██╗████╗  ██║██╔════╝     ██║ ██╔╝██║  ██║██╔══██╗██╔════╝██║  ██║██╔══██╗██╔══██╗██╔══██╗████╗  ██║
██║     ███████║███████║   ██║   ██║   ██║██████╔╝██║   ██║██╔██╗ ██║██║  ███╗    █████╔╝ ███████║███████║██║     ███████║███████║██████╔╝███████║██╔██╗ ██║
██║     ██╔══██║██╔══██║   ██║   ██║   ██║██╔══██╗██║   ██║██║╚██╗██║██║   ██║    ██╔═██╗ ██╔══██║██╔══██║██║     ██╔══██║██╔══██║██╔══██╗██╔══██║██║╚██╗██║
╚██████╗██║  ██║██║  ██║   ██║   ╚██████╔╝██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝    ██║  ██╗██║  ██║██║  ██║╚██████╗██║  ██║██║  ██║██████╔╝██║  ██║██║ ╚████║
 ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝
                                                                                                                                                            
                 
  ```
