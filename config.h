#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

#define SELEC_AUTO 18
#define SELEC_MANUAL 19
#define BUTTON_ON_OFF 25
#define LAMP_STATUS_AUTO 32
#define LAMP_STATUS_ONOFF 33
#define PUMP 2
#define TIME_OFF_LED 30000
#define DEBOUNCE_DELAY 1000
#define DEBOUNCE_TIME 1000
#define DEBOUNCE_DATA 5000

typedef enum {
  AUTO,
  MANUAL,
  NONE
} USE_MODE;


USE_MODE selecMode();
void digitalClockDisplay();
void printDigits(int digits);
void printtime();
void printDate();
void mode_auto();
void mode_manual();
uint8_t updateStopLastHour(uint8_t hourset, uint8_t updateStopLast);
String hexToString(const char *hex);
void Settings();
void onDipLcd();
void reset_none();

#endif
